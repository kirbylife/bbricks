function dump(o)
  s = "X: " .. o.x .. "\tY:" .. o.y
  return s
end

function generate_ball(x, y, angle, dir, radius)
  local vel_x = math.sin(angle) * 100 * dir
  local vel_y = math.cos(angle) * 100
  return {
    x = x,
    y = y,
    vel_x = vel_x,
    vel_y = vel_y,
    radius = radius
  }
end

function love.load()
  love.graphics.setBackgroundColor(0, 0, 0)
  love.window.setMode(500, 700, {resizable=false})
  player_x = 250
  player_y = 640
  flag = false
  is_active = false
  cursor_press = {}
  cursor_move = {}
  angle = 0
  dir = 0
  touch_id = nil
  total_balls = 10
  remaining_balls = 0
  balls = {}
end

function love.draw()
  love.graphics.setColor(180, 180, 180, 255)
  -- love.graphics.circle("fill", ball_x, ball_y, 10)
  love.graphics.circle("fill", player_x, player_y, 10)
  if flag then
    love.graphics.setColor(180, 180, 180, 255)
    love.graphics.print("move " .. dump(cursor_move), 10, 10)
    love.graphics.print("press " .. dump(cursor_press), 10, 30)
    love.graphics.print("angle " .. angle, 10, 50)
    love.graphics.line(cursor_press.x, cursor_press.y, cursor_press.x, cursor_move.y, cursor_move.x, cursor_move.y, cursor_press.x, cursor_press.y)
    local point_x = player_x + (math.sin(angle) * 100) * dir
    local point_y = player_y + (math.cos(angle) * 100)
    love.graphics.line(player_x, player_y, point_x, point_y)
  end
  if active then
    for ball in balls do
      love.graphics.circle("fill", ball.x, ball.y, ball.radius)
    end
  end
end

function love.update(dt)
  if flag then
    if touch_id == nil then
      cursor_move = {x = love.mouse.getX(), y = love.mouse.getY()}
    else
      x, y = love.touch.getPosition(touch_id)
      cursor_move = {x = x, y = y}
    end
    if cursor_move.y <= cursor_press.y then
      angle = 0
    else
      catA = cursor_press.x - cursor_move.x
      catB = cursor_press.y - cursor_move.y
      hypo = math.sqrt(catA * catA + catB * catB)
      angle = math.acos(catB / hypo)
      if cursor_move.x <= cursor_press.x then
        dir = 1
      else
        dir = -1
      end
    end
  else
    angle = 0
    dir = 0
  end
  -- local width, height, _ = love.window.getMode()
  -- if ball_x <= (0 + 5) or ball_x >= (width - 5) then
  --   velocity_x = velocity_x * -1
  -- end
  -- if ball_y <= (0 + 5) then
  --   velocity_y = velocity_y * -1
  -- end
  -- if  ball_y >= (height - 5) then
  --   active = false
  --   player_x = ball_x
  --   ball_y = 400
  -- end
  -- if active then
  --   ball_x = ball_x + velocity_x
  --   ball_y = ball_y + velocity_y
  -- end
end

function love.mousepressed(x, y, button, istouch)
  pressed(x, y)
end

function love.touchpressed(id, x, y, dx, dy, pressure)
  touch_id = id
  pressed(x, y)
end

function love.mousereleased(x, y, button, istouch)
  released()
end

function love.touchreleased(id, x, y, dx, dy, pressure)
  released()
end

function pressed(x, y)
  if not active then
    cursor_press = {x = x, y = y}
    flag = true
  end
end

function released()
  if flag then
    flag = false
    active = true
    remaining_balls = total_balls
  end
end
